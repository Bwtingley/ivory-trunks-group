<?php

	function ivory_preprocess_page(&$variables){

		$page = $variables['page'];
		$content_class = 'three-fourths';
		$sidebar_right = 'one-fourth';
		$variables['page']['tpl_control'] = [];


		if(empty($page['sidebar_right'])){
			$sidebar_right = '';
			$content_class = 'full-width';
		}
		else{ 
			$sidebar_right = 'one-fourth';
			$content_class = 'three-fourths';
		}

		$variables['page']['tpl_control']['content_class'] = $content_class;
		$variables['page']['tpl_control']['sidebar_right'] = $sidebar_right;

	}